#!/usr/bin/env bash
set -e   # set -o errexit
set -u   # set -o nounset
set -o pipefail
[ "x${DEBUG:-}" = "x" ] || set -x

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

PROJECT_ID=
DEPLOYMENT_EXIST=
SERVICE_ACCOUNT_NAME=cem-service-account
SERVICE_ACCOUNT_ID=cyb-cem
SERVICE_ACCOUNT_CONFIG_FILE=cem_service_account.yml
CEM_SINK_NAME=cem-sink
CEM_DATASET_NAME=cem_logs_dataset
CUSTOM_CEM_ROLE_CONFIG_FILE=cem_custom_role_config.yml

function Setup
{
    # Get user input
    while getopts p:hs o
    do  case "$o" in
      p)  PROJECT_ID="$OPTARG";;
      [?] | h) ShowUsage ; exit 1;;
      esac
    done

    # Set cloud shell project to project_id
    gcloud config set project ${PROJECT_ID} > /dev/null 2>&1

    if [[ "${PROJECT_ID}" == "PROJECT_ID" ]]; then
      ShowUsage
      exit 1
    fi

    if [[ -z "${PROJECT_ID}" ]]; then
      ShowUsage
      exit 1
    fi

    # Get deployment status
    DEPLOYMENT_EXIST=false
    for line in $(gcloud deployment-manager deployments list); do
        if [[ "${line}" == "cem-service-account" ]]; then
          DEPLOYMENT_EXIST=true
        fi
    done
}

function ShowUsage
{
    echo "Usage: $0 -p PROJECT_ID"
}

function EnableAPIs
{
    echo "Enabling deploymentmanager, IAM ,cloudresourcemanager and bigQuery APIs..."
    gcloud services enable deploymentmanager.googleapis.com \
    cloudresourcemanager.googleapis.com \
    iam.googleapis.com \
    bigquery.googleapis.com \
    recommender.googleapis.com \
    policyanalyzer.googleapis.com
}

function AddPermissionsGoogleServiceAccount
{
    echo "Adding google serviceaccount permissions..."
    gserviceaccount=$(gcloud projects get-iam-policy \
    ${PROJECT_ID} | grep -m 1 -Po 'serviceAccount:[0-9]+@cloudservices.gserviceaccount.com')

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member ${gserviceaccount} \
    --role roles/iam.securityAdmin \
    --condition=None  > /dev/null 2>&1

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member ${gserviceaccount} \
    --role roles/iam.roleAdmin \
    --condition=None  > /dev/null 2>&1
}

function CreateOrUpdateCustomCEMRole
{
    # Undelete CustomCEMRole in case it's in deleted status
    gcloud iam roles undelete CustomCEMRole --project=${PROJECT_ID} > /dev/null 2>&1 \
     || echo "CustomCEMRole was enabled or doesn't exist"

    # Create CustomCemRole and in case it exists update CustomCemRole
    gcloud iam roles create CustomCEMRole --project=${PROJECT_ID} --file=${CUSTOM_CEM_ROLE_CONFIG_FILE} > /dev/null 2>&1 \
     || gcloud iam roles update CustomCEMRole --project=${PROJECT_ID} --file=${CUSTOM_CEM_ROLE_CONFIG_FILE} --quiet
}

function CreateOrUpdateDeployment
{
    if [ $DEPLOYMENT_EXIST = true ] ; then
      echo "Update an existing deployment..."
      gcloud deployment-manager deployments update ${SERVICE_ACCOUNT_NAME} --config ${SERVICE_ACCOUNT_CONFIG_FILE}
    else
      echo "Creating CEM service account deployment..."
      gcloud deployment-manager deployments create ${SERVICE_ACCOUNT_NAME} --config ${SERVICE_ACCOUNT_CONFIG_FILE}
    fi

    # Add iam policy binding to the service account
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role projects/${PROJECT_ID}/roles/CustomCEMRole \
    --condition=None  > /dev/null 2>&1

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role roles/bigquery.jobUser \
    --condition=None  > /dev/null 2>&1
}

function CreateOrValidateBigQuery
{
    # Create bigQuery dataset
    echo "Creating bigQuery dataset..."
    bq mk ${CEM_DATASET_NAME}  >/dev/null 2>&1 || echo "$CEM_DATASET_NAME already exists.Continue... "
}

function CreateSink
{
    # Create sink that export all logs tox BigQuery
    echo "Creating Sink with bigQuery destination..."
    gcloud logging sinks create ${CEM_SINK_NAME} \
    bigquery.googleapis.com/projects/${PROJECT_ID}/datasets/${CEM_DATASET_NAME} \
    --log-filter='protoPayload.@type ="type.googleapis.com/google.cloud.audit.AuditLog" AND NOT resource.type: k8s' \
    --quiet > /dev/null 2>&1 || echo "${CEM_SINK_NAME} already exists"

    #Get all details about cem-sink
    cemsinkservice=$(gcloud beta logging sinks describe ${CEM_SINK_NAME} |grep -m 1 -Po 'p[0-9]+-[0-9]+' )

    #Set IAM permission to sink service account
    echo "Adding sink serviceaccount permissions..."
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${cemsinkservice}@gcp-sa-logging.iam.gserviceaccount.com \
    --role roles/bigquery.dataEditor \
    --condition=None >/dev/null 2>&1

    #Set IAM permission to sink service account
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${cemsinkservice}@gcp-sa-logging.iam.gserviceaccount.com \
    --role roles/logging.logWriter \
    --condition=None >/dev/null 2>&1

    #Update sink with the new roles
    echo "Update sink..."
    gcloud logging sinks update  ${CEM_SINK_NAME} bigquery.googleapis.com/projects/${PROJECT_ID}/datasets/${CEM_DATASET_NAME}
}

function GetFinalValue
{
    echo Trying to get the final value
    manifest=$(gcloud deployment-manager deployments describe $SERVICE_ACCOUNT_NAME|grep -m 1 -Po 'manifest-[0-9]+')
    finalValue=$(gcloud deployment-manager manifests describe $manifest --deployment $SERVICE_ACCOUNT_NAME | grep finalValue|cut -d":" -f2 |cut -d" " -f2)
    echo Your final value is:
    echo $finalValue
}

function RunDeploymentSteps
{
    EnableAPIs
    AddPermissionsGoogleServiceAccount
    CreateOrUpdateCustomCEMRole
    CreateOrUpdateDeployment
    CreateOrValidateBigQuery
    CreateSink
}

Setup "$@"
RunDeploymentSteps
GetFinalValue
